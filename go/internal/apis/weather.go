// Tout ce qui permet d'obtenir des informations météorologique
// depuis l'api de https://www.weatherapi.com/
package apis

import (
	"encoding/json"
	"io"
	"net/http"
)

type apiData struct {
	Location struct {
		Name           string  `json:"name"`
		Region         string  `json:"region"`
		Country        string  `json:"country"`
		Lat            float64 `json:"lat"`
		Lon            float64 `json:"lon"`
		TzID           string  `json:"tz_id"`
		LocaltimeEpoch float64 `json:"localtime_epoch"`
		Localtime      string  `json:"localtime"`
	} `json:"location"`
	Current struct {
		LastUpdatedEpoch int     `json:"last_updated_epoch"`
		LastUpdated      string  `json:"last_updated"`
		TempC            float64 `json:"temp_c"`
		TempF            float64 `json:"temp_f"`
		IsDay            int     `json:"is_day"`
		Condition        struct {
			Text string `json:"text"`
			Icon string `json:"icon"`
			Code int    `json:"code"`
		} `json:"condition"`
		WindKph    float64 `json:"wind_kph"`
		WindDegree float64 `json:"wind_degree"`
		WindDir    string  `json:"wind_dir"`
		PressureMb float64 `json:"pressure_mb"`
		PressureIn float64 `json:"pressure_in"`
		PrecipMm   float64 `json:"precip_mm"`
		PrecipIn   float64 `json:"precip_in"`
		Humidity   float64 `json:"humidity"`
		Cloud      float64 `json:"cloud"`
		FeelslikeC float64 `json:"feelslike_c"`
		FeelslikeF float64 `json:"feelslike_f"`
		VisKm      float64 `json:"vis_km"`
		VisMiles   float64 `json:"vis_miles"`
		Uv         float64 `json:"uv"`
		GustMph    float64 `json:"gust_mph"`
		GustKph    float64 `json:"gust_kph"`
	} `json:"current"`
}

type LocalData struct {
	Name        string  `json:"ville"`
	Region      string  `json:"region"`
	Country     string  `json:"pays"`
	Localtime   string  `json:"date"`
	LastUpdated string  `json:"mise_a_jour"`
	TempC       float64 `json:"temperature"`
	Text        string  `json:"description"`
	Icon        string  `json:"icone"`
	WindKph     float64 `json:"vitesse_du_vent"`
}

func GetWeatherData(apiKey string) (*LocalData, error) {
	url := "https://api.weatherapi.com/v1/current.json?q=Caen&aqi=no&key=" + apiKey
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	body, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	var weatherData apiData
	err = json.Unmarshal(body, &weatherData)
	if err != nil {
		return nil, err
	}

	sanitizedData := &LocalData{
		Name:        weatherData.Location.Name,
		Region:      weatherData.Location.Region,
		Country:     weatherData.Location.Country,
		Localtime:   weatherData.Location.Localtime,
		LastUpdated: weatherData.Current.LastUpdated,
		TempC:       weatherData.Current.TempC,
		Text:        weatherData.Current.Condition.Text,
		Icon:        weatherData.Current.Condition.Icon,
		WindKph:     weatherData.Current.WindKph,
	}

	return sanitizedData, nil
}
