package models

import (
	"database/sql"
	"time"
)

type Report struct {
	ID       int
	Reporter string
	Created  time.Time
}

// Définition d'un ReportRepository qui emballe un pool de connexion sql.DB.
type ReportRepository struct {
	DB *sql.DB
}

// Insertion d'un rapport en base de donnée
func (r *ReportRepository) Insert(reporter string) (int, error) {
	query := `
		INSERT INTO reports (reporter, created)
		VALUES(?, UTC_TIMESTAMP())
	`
	result, err := r.DB.Exec(query, reporter)
	if err != nil {
		return 0, err
	}
	id, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}

// Retourne tous les rapports en base par ordre de création
func (r *ReportRepository) All() ([]*Report, error) {
	query := `
		SELECT reporter, created
		FROM reports
		ORDER BY created ASC
	`
	rows, err := r.DB.Query(query)
	if err != nil {
		return nil, err
	}
	reports := []*Report{}
	for rows.Next() {
		rp := &Report{}
		err = rows.Scan(&rp.Reporter, &rp.Created)
		if err != nil {
			return nil, err
		}
		reports = append(reports, rp)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}

	return reports, nil
}
