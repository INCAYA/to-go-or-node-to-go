package main

import (
	"database/sql"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"
	"text/template"
	"time"

	_ "github.com/go-sql-driver/mysql"

	"go.incaya.com/internal/apis"
	"go.incaya.com/internal/models"
)

func humanDate(t time.Time) string {
	return t.Format("02/01/2006")
}

var templateFunctions = template.FuncMap{
	"humanDate": humanDate,
}

type templateData struct {
	CurrentYear int
	Reports     []*models.Report
}

type application struct {
	errorLog *log.Logger
	infoLog  *log.Logger
	apiKey   string
	reports  *models.ReportRepository
}

func main() {
	const addr string = "0.0.0.0:8001"

	fileInfo, err := os.OpenFile("/clogs/info.log", os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		log.Fatal(err)
	}
	defer fileInfo.Close()
	infoWriters := io.MultiWriter(os.Stdout, fileInfo)
	infoLog := log.New(infoWriters, "[info]\t", log.Ldate|log.Ltime)

	errorInfo, err := os.OpenFile("/clogs/error.log", os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		log.Fatal(err)
	}
	defer errorInfo.Close()
	errorWriters := io.MultiWriter(os.Stdout, errorInfo)
	errorLog := log.New(errorWriters, "[error]\t", log.Ldate|log.Ltime|log.Lshortfile)

	dsn := "nodego:password@tcp(db:3306)/gonode?parseTime=true"
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		errorLog.Fatal(err)
	}
	if err = db.Ping(); err != nil {
		errorLog.Fatal(err)
	}
	infoLog.Print("Connected to Mysql")

	defer db.Close()

	app := &application{
		errorLog: errorLog,
		infoLog:  infoLog,
		apiKey:   os.Getenv("WEATHER_API_KEY"),
		reports:  &models.ReportRepository{DB: db},
	}

	fileServer := http.FileServer(http.Dir("./ui/assets/"))
	mux := http.NewServeMux()
	mux.Handle("/assets/", http.StripPrefix("/assets", fileServer))
	mux.HandleFunc("/", app.home)
	mux.HandleFunc("/signaler", app.report)
	mux.HandleFunc("/a-propos", app.about)
	mux.HandleFunc("/api/weather", app.weather)

	infoLog.Printf("Go server running on %s", addr)
	srv := &http.Server{
		Addr:     addr,
		ErrorLog: errorLog,
		Handler:  mux,
	}
	error := srv.ListenAndServe()
	errorLog.Fatal(error)
}

func (app *application) home(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}

	files := []string{
		"./ui/base.html",
		"./ui/pages/home.html",
		"./ui/partials/footer.html",
		"./ui/partials/header.html",
	}

	ts, err := template.ParseFiles(files...)
	if err != nil {
		app.errorLog.Print(err.Error())
		http.Error(w, "Internal Server Error", 500)
		return
	}

	data := &templateData{
		CurrentYear: time.Now().Year(),
	}
	err = ts.ExecuteTemplate(w, "base", data)
	if err != nil {
		app.errorLog.Print(err.Error())
		http.Error(w, "Internal Server Error", 500)
	}
}

func (app *application) report(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		err := r.ParseForm()
		if err != nil {
			app.errorLog.Print(err.Error())
			http.Error(w, "Internal Server Error", http.StatusBadRequest)
			return
		}

		_, error := app.reports.Insert(r.PostForm.Get("nom"))
		if error != nil {
			app.errorLog.Print(err.Error())
			http.Error(w, "Internal Server Error", http.StatusBadRequest)
			return
		}

		http.Redirect(w, r, "/signaler", http.StatusSeeOther)
		return
	}

	files := []string{
		"./ui/base.html",
		"./ui/pages/report.html",
		"./ui/partials/footer.html",
		"./ui/partials/header.html",
	}

	ts, err := template.New("reportPage").Funcs(templateFunctions).ParseFiles(files...)
	if err != nil {
		app.errorLog.Print(err.Error())
		http.Error(w, "Internal Server Error", 500)
		return
	}

	reports, err := app.reports.All()
	if err != nil {
		app.errorLog.Print(err.Error())
		http.Error(w, "Internal Server Error", http.StatusBadRequest)
		return
	}

	data := &templateData{
		CurrentYear: time.Now().Year(),
		Reports:     reports,
	}

	err = ts.ExecuteTemplate(w, "base", data)
	if err != nil {
		app.errorLog.Print(err.Error())
		http.Error(w, "Internal Server Error", 500)
	}

}

func (app *application) about(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.Header().Set("Allow", http.MethodGet)
		http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
		return
	}

	files := []string{
		"./ui/base.html",
		"./ui/pages/about.html",
		"./ui/partials/footer.html",
		"./ui/partials/header.html",
	}

	ts, err := template.ParseFiles(files...)
	if err != nil {
		app.errorLog.Print(err.Error())
		http.Error(w, "Internal Server Error", 500)
		return
	}

	data := &templateData{
		CurrentYear: time.Now().Year(),
	}
	err = ts.ExecuteTemplate(w, "base", data)
	if err != nil {
		app.errorLog.Print(err.Error())
		http.Error(w, "Internal Server Error", 500)
	}
}

func (app *application) weather(w http.ResponseWriter, r *http.Request) {
	contentType := r.Header.Get("Content-type")
	if contentType != "application/json" {
		w.Header().Set("Accept", "application/json")
		http.Error(w, "Not Acceptable", http.StatusNotAcceptable)
		return
	}

	weatherData, err := apis.GetWeatherData(app.apiKey)
	if err != nil {
		app.errorLog.Print("Call for weatherapi made with error on")
		app.errorLog.Print(err.Error())
		http.Error(w, "Internal Server Error", 500)
		return
	}
	dataInJson, err := json.Marshal(weatherData)
	if err != nil {
		app.errorLog.Print("Transform data from weatherapi with error")
		app.errorLog.Print(err.Error())
		http.Error(w, "Internal Server Error", 500)
		return
	}
	app.infoLog.Print("Call for weatherapi made with no error")
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(string(dataInJson)))
}
