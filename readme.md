# To Go or Node to Go

![logo](logo.png)
Un repo avec quelques exemples didactiques visant à aider au choix de Node.js ou Go comme premier langage d'apprentissage de programmation backend.

## Prérequis

Pour pouvoir lancer les exemples, vous devrez avoir installé au préalable :
- [Docker](https://docs.docker.com/get-docker/) avec le plugin [Docker Compose](https://docs.docker.com/compose/),
- [Just](https://just.systems/) si vous souhaitez profiter des recettes du fichier `justfile`.

Si vous voulez faire fonctionner la demo jusqu'au bout, vous devrez également obtenir une clé d'API sur le site [WeatheAPI](https://www.weatherapi.com/) et l'ajouter dans vos variables d'environnement sous la clé `WEATHER_API_KEY`.
