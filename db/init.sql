CREATE TABLE reports (
id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
reporter TEXT NOT NULL,
created DATETIME NOT NULL
);

CREATE INDEX idx_reports_created ON reports(created);

INSERT INTO reports (reporter, created) VALUES ('Becky Chambers','2023-11-01');
INSERT INTO reports (reporter, created) VALUES ('Rivers Solomon','2023-11-02');
INSERT INTO reports (reporter, created) VALUES ('Alain Damasio','2023-11-03');
