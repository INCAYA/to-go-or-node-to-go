import querystring from 'node:querystring'; 

export const parseForm = (request, callback) => {
    const FORM_URLENCODED = 'application/x-www-form-urlencoded';
    if(request.headers['content-type'] === FORM_URLENCODED) {
        let body = '';
        request.on('data', chunk => {
            body += chunk.toString();
        });
        request.on('end', () => {
            callback(querystring.parse(body));
        });
    }
    else {
        callback(null);
    }
}
