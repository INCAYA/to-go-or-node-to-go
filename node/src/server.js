import http from "node:http";
import mysql from "mysql";

import staticServer from "./static-server.js";
import getHtmlForPage from "./templates/index.js";
import logger from "./logger.js";
import { parseForm } from "./form.js";

const hostname = "0.0.0.0";
const port = 8002;
const apiKey = process.env.WEATHER_API_KEY;
const weatherApiBasePath = "https://api.weatherapi.com/v1";

const poolDB  = mysql.createPool({
  connectionLimit : 10,
  host            : 'db',
  user            : 'nodego',
  password        : 'password',
  database        : 'gonode'
});

const server = http.createServer();

server.on("request", async (request, response) => {
  if (request.url.includes("assets")) {
    return staticServer(request, response);
  }

  switch (request.url) {
    case "/":
      return home(response);
    case "/signaler":
      return report(request, response, poolDB);
    case "/a-propos":
      return about(request, response);
    case "/api/weather":
      return weather(request, response);
  }

  logger.error(`Unknow url ${request.url}`);
  response.setHeader("Content-Type", "text/plain; charset=utf-8");
  response.statusCode = 404;
  response.end("Not found");
});

server.listen(port, hostname, () => {
  logger.info(`Node server running at http://${hostname}:${port}/`);
});

const home = (response) => {
  response.setHeader("Content-Type", "text/html; charset=utf-8");
  response.end(getHtmlForPage("home"));
};

const report = (request, response, poolDB) => {
  if (request.method === "POST") {
    parseForm(request, (formData) => {
      poolDB.getConnection((err, connection) => {
        if (err) {
          logger.error(`Unable to connect DB: ${err.message}`);
          response.setHeader("Content-Type", "text/plain; charset=utf-8");
          response.statusCode = 500;
          return response.end("Internal Server Error");
        }
  
        connection.query({
          sql: `
            INSERT INTO reports (reporter, created)
            VALUES(?, UTC_TIMESTAMP())
          `,
          values: [formData.nom]
        }, (error) => {
          connection.release();
          if (error) {
            logger.error(`Unable to insert data in DB: ${error.message}`);
            response.setHeader("Content-Type", "text/plain; charset=utf-8");
            response.statusCode = 500;
            return response.end("Internal Server Error");
          }
  
          response.statusCode = 301;
          response.setHeader('Location', '/signaler');
          return response.end();
        });
      });
    });
  } else {
    poolDB.getConnection((err, connection) => {
      if (err) {
        logger.error(`Unable to connect DB: ${err.message}`);
        response.setHeader("Content-Type", "text/plain; charset=utf-8");
        response.statusCode = 500;
        return response.end("Internal Server Error");
      }

      connection.query(`
        SELECT reporter, created FROM reports ORDER BY created ASC
      `, (error, results) => {
        connection.release();
        if (error) {
          logger.error(`Unable to connect DB: ${error.message}`);
          response.setHeader("Content-Type", "text/plain; charset=utf-8");
          response.statusCode = 500;
          return response.end("Internal Server Error");
        }

        response.setHeader("Content-Type", "text/html; charset=utf-8");
        return response.end(getHtmlForPage("report", { reports: results}));
      });
    });
  }
};

const about = (request, response) => {
  if (request.method !== "GET") {
    logger.error("Wrong method on about page");
    response.setHeader("Content-Type", "text/plain; charset=utf-8");
    response.setHeader("Allow", "GET");
    response.statusCode = 405;
    return response.end("Method Not Allowed");
  }

  response.setHeader("Content-Type", "text/html; charset=utf-8");
  response.end(getHtmlForPage("about"));
};

const weather = (request, response) => {
  if (request.headers["content-type"] !== "application/json") {
    logger.error("Wrong asked content type");
    response.setHeader("Accept", "application/json");
    response.statusCode = 406;
    return response.end("Not Acceptable");
  }

  const url = `${weatherApiBasePath}/current.json?q=Caen&aqi=no&key=${apiKey}`;
  fetch(url)
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(
        `Error on weatherapi request with code ${response.status}`
      );
    })
    .then((json) => {
      logger.info(`Call for weatherapi made with no error`);
      response.setHeader("Content-Type", "application/json; charset=utf-8");
      return response.end(
        JSON.stringify({
          ville: json.location.name,
          region: json.location.region,
          pays: json.location.country,
          date: json.location.localtime,
          mise_a_jour: json.current.last_updated,
          temperature: json.current.temp_c,
          description: json.current.condition.text,
          icone: json.current.condition.icon,
          vitesse_du_vent: json.current.wind_kph,
        })
      );
    })
    .catch(function (err) {
      logger.error(`Unable to join api on ${url}: ${err.message}`);
      response.setHeader("Content-Type", "text/plain; charset=utf-8");
      response.statusCode = 500;
      return response.end("Internal Server Error");
    });
};
