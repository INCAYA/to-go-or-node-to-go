// from https://developer.mozilla.org/en-US/docs/Learn/Server-side/Node_server_without_framework
import fs from "node:fs";
import path from "node:path";

const MIME_TYPES = {
  default: "application/octet-stream",
  html: "text/html; charset=UTF-8",
  js: "application/javascript",
  json: "application/json",
  css: "text/css",
  png: "image/png",
  jpg: "image/jpg",
  gif: "image/gif",
  ico: "image/x-icon",
  svg: "image/svg+xml",
};

const ASSSETS_PATH = path.join(process.cwd(), "./src/assets");

const toBool = [() => true, () => false];

const prepareFile = async (url) => {
  const paths = [ASSSETS_PATH, url.replace('/assets/', '')];
  if (url.endsWith("/")) paths.push("index.html");
  const filePath = path.join(...paths);
  const pathTraversal = !filePath.startsWith(ASSSETS_PATH);
  const exists = await fs.promises.access(filePath).then(...toBool);
  const found = !pathTraversal && exists;
  const streamPath = found ? filePath : ASSSETS_PATH + "/404.html";
  const ext = path.extname(streamPath).substring(1).toLowerCase();
  const stream = fs.createReadStream(streamPath);
  return { found, ext, stream };
};

const staticServer = async (request, response) => {
    const file = await prepareFile(request.url);
    const statusCode = file.found ? 200 : 404;
    const mimeType = MIME_TYPES[file.ext] || MIME_TYPES.default;
    response.writeHead(statusCode, { "Content-Type": mimeType });
    file.stream.pipe(response);
}

export default staticServer;
