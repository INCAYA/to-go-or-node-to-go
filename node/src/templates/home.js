export const getHomeContent = () => `
<main class="container">
    <article>
        <div class="grid">
            <div>
                <h2>De la donnée en JSON</h2>
                <p>On requête de la donnée depuis une API externe. On ne fait pas d'appel direct à cette API pour
                    ne pas exposer une clé d'API nécessaire à l'utilisation du service.<br/>
                    C'est pourquoi on passe par le back !</p>
                <p><button id="json-fetcher">Obtenir la donnée</button></p>
            </div>
            <div>
                    <pre>
                        <code id="json-container">En attente de la donnée ...</code>
                    </pre>
            </div>
            <script>
                window.addEventListener('load', function () {
                    document.getElementById('json-fetcher').addEventListener('click', () => {
                        document.getElementById('json-container').textContent = "";
                        let request = new Request("/api/weather", {
                            method: "GET",
                            headers: {
                                "Content-Type": "application/json",
                            },
                            cache: "default",
                        });

                        fetch(request)
                            .then(function (response) {
                                if (!response.ok) {
                                    throw new Error("erreur HTTP! statut: " + response.status);
                                }
                                return response.json();
                            })
                            .then(function (json) {
                                const textContent = JSON.stringify(json, null, " ");
                                document.getElementById('json-container').textContent = textContent;
                            });
                    })
                })
            </script>
        </div>
    </article>
</main>
`;
