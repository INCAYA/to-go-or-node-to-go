export const getReportContent = (data) => {
  return `
<main class="container">
    <h1>Signaler</h1>
    <div class="grid">
        <div>
            <form  action="/signaler" method="post">
                <label for="nom">Votre nom *</label>
                <input type="text" id="nom" name="nom" placeholder="Par exemple N.K. Jemisin" required>
                <small>Nous ne le divulguerons sans doute pas ...</small>
                <button type="submit">Soumettre</button>
            </form>
        </div>
        <div style="padding: 0 2rem">
            <h2>Les signalements</h2>
            <ul>
                ${displayReports(data.reports)}
            </ul>
        </div>
    </div>
</main>
`;
};

const displayReports = (reports) => {
  if (reports && reports.length) {
    return reports.reduce((acc, report) => {
      return `${acc}<li>Signalement du ${new Date(report.created).toLocaleDateString('fr-FR')} par <b>${report.reporter}</b></li>`
    }, '');
  }

  return "";
};
