export const getHeader = () => `
<header class="container">
    <nav>
        <ul>
            <li>
                <a href="/" style="color: black">
                    <img width="60" height="60" src="/assets/logo.png" alt="project's logo"/><strong> > Node version</strong>
                </a>
            </li>
        </ul>
        <ul id="mainNavList">
            <li><a href="/">Acceuil</a></li>
            <li><a href="/signaler">Signaler</a></li>
            <li><a href="/a-propos">A propos</a></li>
        </ul>
    </nav>
    <script>
        window.addEventListener("load", function () {
            const menuEntries = Array.from(document.getElementById('mainNavList').children);
            const currentPage = document.location.pathname;
            for (const entry of menuEntries) {
                if (entry.firstChild.attributes.href.value === currentPage) {
                    entry.firstChild.setAttribute('role', 'button');
                    break;
                }
            }
        })
    </script>
</header>
`;

export const getFooter = (year) => `
<footer class="container">to Go or Node to Go @INCAYA ${year}</footer>
`;
