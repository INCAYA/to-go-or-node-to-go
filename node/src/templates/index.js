import { getFooter, getHeader } from "./partials.js";
import { getHomeContent } from "./home.js";
import { getAboutContent } from "./about.js";
import { getReportContent } from "./report.js";

const getHtmlForPage = (pageName, data = {}) => {
  const now = new Date();
  const title =
    pageName === "home"
      ? "Acceuil"
      : pageName === "about"
      ? "A propos"
      : "Signaler";
  const pageContent =
    pageName === "home"
      ? getHomeContent()
      : pageName === "about"
      ? getAboutContent()
      : getReportContent(data)

  return `
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>${title} - Node</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel='shortcut icon' href='/assets/favicon.ico' type='image/x-icon'>
    <link rel="stylesheet" href="/assets/style.css">
</head>
<body>
${getHeader()}
${pageContent}
${getFooter(now.getFullYear())}
</body>
</html>
`;
};

export default getHtmlForPage;
