import fs from 'node:fs';

const infoLog = fs.createWriteStream('/clogs/info.log', {flags : 'w'});
const ErrorLog = fs.createWriteStream('/clogs/error.log', {flags : 'w'});
const infoOutput = process.stdout;
const errorOutput = process.stderr;

const getDate = () => {
    const now = new Date();
    return `${now.toLocaleDateString('fr-fr')} ${now.toLocaleTimeString('fr-fr')}`;
}

// the flag 'a' will update the stream log at every launch
const info  = (msg) => { 
    const log = `[info]\t${getDate()}\t${msg}\n`;
    infoLog.write(log);
    infoOutput.write(log);
};


const error = (msg) => {
    const log = `[error]\t${getDate()}\t${msg}\n`;
    ErrorLog.write(log);
    errorOutput.write(log);
}

export default {
    info,
    error
}
