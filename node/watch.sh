#!/bin/sh
node src/server.js &
PID=$!
watch () {
    inotifywait -r -m -e close_write "src" | while read f; do kill $PID; node src/server.js & PID=$!; done
}

watch