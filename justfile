install:
  @docker compose run --no-deps --rm node bash -ci 'npm install'
start:
  @docker compose run --no-deps --rm go bash -ci 'go mod download'
  @docker compose run --no-deps --rm go bash -ci 'go mod verify'
  @docker compose up -d
stop:
  @docker compose down -t 1
log:
  @docker compose logs -f
reload:
  @docker compose restart go
